import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import Navigation from './components/Navigation'
import Albums from './components/Albums'
import CurrentAlbum from './components/CurrentAlbum'
import LikedTrack from './components/LikedTrack'
import Tracks from './components/Track'
import {connect} from "react-redux";
import {loadAlbums} from "./actions/albumsAction";
import {loadTracks} from "./actions/tracksAction";
import {likedLoad} from "./actions/likedTracksAction";
class App extends Component {
    componentDidMount(){
        if(this.props.albums.length === 0 || this.props.tracks.length === 0 || this.props.liked.length === 0){
            this.props.loadData();
        }
    }
    render () {
        return (
            <div>
                <Navigation/>
                <Switch>
                    <Route exact path="/albums" component={Albums}/>
                    <Route exact path="/tracks" component={Tracks}/>
                    <Route exact path={`/albums/:id`} component={CurrentAlbum}/>
                    <Route exact path={`/tracks/liked`} component={LikedTrack}/>
                    <Route path="/" component={Albums}/>
                </Switch>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    albums: state.albums,
    tracks: state.tracks,
    like: state.like
})
const mapDispatchToProps = (dispatch) =>({
    loadData: () => {
        dispatch(loadAlbums())
        dispatch(loadTracks())
        dispatch(likedLoad())
    }
})
export default connect(mapStateToProps, mapDispatchToProps) (App)
