import {ALBUMS_LOADED} from "./types";
export const loadAlbums = () => dispatch => {
    const url = 'http://localhost:8080/api/albums'
    fetch(url)
        .then(res => res.json())
        .then(data => dispatch({type: ALBUMS_LOADED, payload: data}))
}