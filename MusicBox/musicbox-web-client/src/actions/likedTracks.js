import {LIKED_TRACKS_LOADED} from "./types";
export const likedLoad = () => dispatch => {
    const url = 'http://localhost:8080/api/tracks'
    fetch(url)
        .then(res => res.json())
        .then(data => dispatch({type: LIKED_TRACKS_LOADED, payload: data}))
};