import {TRACKS_LOADED} from "./types";

export const loadTracks = () => dispatch => {
    const url = 'http://localhost:8080/api/tracks'
    fetch(url)
        .then(res => res.json())
        .then(data => dispatch({type: TRACKS_LOADED, payload: data}))
};