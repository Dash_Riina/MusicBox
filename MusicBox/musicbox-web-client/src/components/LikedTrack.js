import React from 'react'
import { Link } from 'react-router-dom'
import {connect} from "react-redux";
class LikedTrack extends React.Component {
    render(){
        const length = this.props.like.length
        return (
            <div>
                <Link to="/tracks/liked">
                    <button className="btn btn-info">
                        Liked
                        <span className="badge badge-info">{length}</span>
                    </button>
                </Link>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    like: state.like
})
export default connect(mapStateToProps) (LikedTrack)