import React from 'react'
class Login extends React.Component {
    constructor () {
        super()
        this.state = {isShow: false}
    }
    changeState = () => {
        this.setState({isShow: !this.state.isShow})
    }
    render () {
        let showMe
        if (this.state.isShow) {
            showMe = 'show'
        }
        return (
            <div className={`dropdown ${showMe} ml-auto`}>
                <button onClick={this.changeState} aria-expanded="true"
                        className="btn btn-outline-info dropdown-toggle float-right" type="button"
                        data-toggle="dropdown" aria-haspopup="true">
                    Login
                </button>
                <div className={`dropdown-menu ${showMe}`}
                     aria-labelledby="dropdownMenuButton">
                    <a className="dropdown-item" href="#">Sign In</a>
                    <a className="dropdown-item" href="#">Sign Up</a>
                </div>
            </div>
        )
    }
}
export default Login