import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import {connect} from "react-redux";
class Albums extends Component {
    render () {
        return (
            <div className='list-group'>
                {this.props.albums.map((album, index) =>
                    <Link className="list-group-item list-group-item-action list-group-item-light"
                          key={index} to={`/albums/${album.album_id}`}>
                        <img className="img-fluid img-thumbnail rounded w-25"
                             src={`http://localhost:8080/${album.album_cover}`} alt={album.album_cover}/>
                        <h3 className="d-inline">{album.album_name}</h3>
                    </Link>)}
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    albums: state.albums
})
export default connect(mapStateToProps) (Albums)