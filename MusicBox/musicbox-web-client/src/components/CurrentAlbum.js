import React from 'react'

class CurrentAlbum extends React.Component {

    render () {
        const id = this.props.match.params.id
        return (
            <div>
                Current Album Number = {id}
            </div>
        )
    }
}

export default CurrentAlbum