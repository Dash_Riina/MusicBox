import {createStore, applyMiddleware} from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk'
function rootReducer (state={}, action){
    return state}
const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))

export default store;