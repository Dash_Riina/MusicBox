import albumReducer from './albumReducer'
import { combineReducers } from 'redux'
import trackReducer from './trackReducer'
import likedTrackReducer from './likedTrackReducer'

const rootReducer = combineReducers({
    albums: albumReducer,
    tracks: trackReducer,
    like: likedTrackReducer
    })
export default rootReducer

