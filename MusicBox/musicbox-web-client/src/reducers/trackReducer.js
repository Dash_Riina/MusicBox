const trackState = []

function trackReducer(state = trackState, action) {
    if (action.type === 'TRACKS_LOADED') {
        return [...action.payload]
    }
    return state
}

export default trackReducer