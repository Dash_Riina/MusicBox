const albumState = []

function albumReducer(state = albumState, action) {
    if (action.type === 'ALBUMS_LOADED' ) {
        return [...action.payload]
    }
    return state
}

export default albumReducer