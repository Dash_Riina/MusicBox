const likedState = []

function likedTrackReducer(state = likedState, action) {
    if (action.type === 'LIKED_TRACK_LOADED') {
        return [...action.payload]
    }
    return state
}

export default likedTrackReducer