package ua.danit.controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class MessageController {

    @GetMapping("/api/messages")
    public String getMessage(Principal principal) {
      String pattern = "This message for authorized users only! You logged in as %s.";
      return String.format(pattern, principal.getName());
    }
  }
