package ua.danit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ua.danit.dao.AlbumDao;
import ua.danit.entity.Album;

import java.util.List;

@RestController
public class AlbumController {

  @Autowired
  private AlbumDao albumDao;

  @RequestMapping(path = "/api/albums", method = RequestMethod.GET)
  public List<Album> getAlbums(){
    return (List<Album>) albumDao.findAll();
  }

  @RequestMapping(path = "/api/albums/{id}", method = RequestMethod.GET)
  public Album getById(@PathVariable long id) {
//    return albumDao.findById(id).get();
  return albumDao.findOne(id);
  }

  @RequestMapping(path = "/api/albums/{id}", method = RequestMethod.PUT)
  public void add(@RequestBody Album album) {
    albumDao.save(album);
  }

  @RequestMapping(path = "/api/albums/{id}", method = RequestMethod.DELETE)
  public void removeById(@PathVariable("id") long id) {
//    albumDao.deleteById(id);
    albumDao.delete(id);
  }

  @RequestMapping(path = "api/albums/{id}/cover", method = RequestMethod.DELETE)
  public void removeCover(@PathVariable("id") long id) {
    albumDao.delete(id);
  }
}
