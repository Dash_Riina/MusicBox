package ua.danit.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
  @Autowired
  private UserDetailsService userDetailsServiceImpl;
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    /*http
        .csrf().disable()
        //.exceptionHandling()
        //.authenticationEntryPoint(new ForbiddenAuthenticationEntryPoint())
        //.and()
        .authorizeRequests()
        .antMatchers("/api/**").authenticated()
        .anyRequest().permitAll()
        .and()
        .formLogin().loginPage("/login").permitAll()
        .successHandler(new MySavedRequestAwareAuthenticationSuccessHandler())
        .failureHandler(new SimpleUrlAuthenticationFailureHandler())
        .and()
        .logout();*/

    http
        .authorizeRequests()
        .antMatchers("/login").permitAll()
        .antMatchers("/").permitAll()
        .antMatchers("/secured/**").authenticated()
        .anyRequest().hasRole("USER")
        .and()
        .formLogin().permitAll()
        .failureUrl("/login-error").permitAll()
        .successForwardUrl("/");

    http.csrf().disable();
  }
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsServiceImpl);
        //.passwordEncoder(new BCryptPasswordEncoder());
  }
  static class ForbiddenAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(
        HttpServletRequest request,
        HttpServletResponse response,
        AuthenticationException authException) throws IOException, ServletException {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
    }
  }
  static class MySavedRequestAwareAuthenticationSuccessHandler
      extends SimpleUrlAuthenticationSuccessHandler {
    private RequestCache requestCache = new HttpSessionRequestCache();
    @Override
    public void onAuthenticationSuccess(
        HttpServletRequest request,
        HttpServletResponse response,
        Authentication authentication)
        throws ServletException, IOException {

      SavedRequest savedRequest
          = requestCache.getRequest(request, response);

      if (savedRequest == null) {
        clearAuthenticationAttributes(request);
        return;
      }
      String targetUrlParam = getTargetUrlParameter();
      if (isAlwaysUseDefaultTargetUrl()
          || (targetUrlParam != null
          && StringUtils.hasText(request.getParameter(targetUrlParam)))) {
        requestCache.removeRequest(request, response);
        clearAuthenticationAttributes(request);
        return;
      }
      clearAuthenticationAttributes(request);
    }

    public void setRequestCache(RequestCache requestCache) {
      this.requestCache = requestCache;
    }
  }
}
