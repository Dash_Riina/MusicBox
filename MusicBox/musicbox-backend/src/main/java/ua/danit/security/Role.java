//package ua.danit.security;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class Role {
//
//    String username;
//    String password;
//    List<String> authorities = new ArrayList<>();
//
//    String getUsername() {
//      return username;
//    }
//
//    Role setUsername(String username) {
//      this.username = username;
//      return this;
//    }
//
//    String getPassword() {
//      return password;
//    }
//
//    Role setPassword(String password) {
//      this.password = password;
//      return this;
//    }
//
//    String[] getAuthorities() {
//      return authorities.toArray(new String[authorities.size()]);
//    }
//
//    Role setAuthorities(String authority) {
//      this.authorities.add(authority);
//      return this;
//    }
//
//    @Override
//    public String toString() {
//      return "Role{" +
//          "username='" + username + '\'' +
//          ", password='" + password + '\'' +
//          ", authorities=" + authorities +
//          '}';
//    }
//  }
