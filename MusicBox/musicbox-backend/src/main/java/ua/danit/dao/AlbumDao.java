package ua.danit.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import ua.danit.entity.Album;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

public interface AlbumDao extends CrudRepository<Album, Long> {


//  private final EntityManager em;
//
//  @Autowired
//  public AlbumDao(EntityManager em) {
//    this.em = em;
//  }
//
//  @Transactional
//  public List<Album> getAllAlbums() {
//    String sql = "select a from Album a";
//    Query query = em.createQuery(sql, Album.class);
//    return query.getResultList();
//  }
}