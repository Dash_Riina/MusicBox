package ua.danit.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ua.danit.entity.User;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.List;

@Component
  public class UserDetailsServiceImpl implements UserDetailsService {

    private final JdbcTemplate jdbc;
    private final String SQL = "select u.name as username, " +
        " u.password as password, r.role as role " +
        " from users u " +
        " inner join user_roles ur on(u.id=ur.user_id) " +
        " inner join roles r " +
        " on(ur.role_id=r.role_id) " +
        " where u.name=?;";
    @Autowired
    public UserDetailsServiceImpl(DataSource dataSource) {
      this.jdbc = new JdbcTemplate(dataSource);
    }
    @Override
    public UserDetails loadUserByUsername(String username)
        throws UsernameNotFoundException {
      final User user = new User();
      user.setUsername("admin");
      user.setPassword("admin123");


      Object users = jdbc.queryForList("SELECT * from users");

      jdbc.query(SQL, new Object[]{username}, (rs, rowNum) ->
          user.setUsername(rs.getString("username"))
              .setPassword(rs.getString("password"))
              .setAuthorities(rs.getString("role")));
      System.out.println(user);


      return new org.springframework.security.core.userdetails.User("afwef", "Fawef", Arrays.asList(new SimpleGrantedAuthority("ROLE_USER")));
    }
  }
