package ua.danit.entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


public class User {
  String username;
  String password;
  List<String> authorities = new ArrayList<>();

  public String getUsername() {
    return username;
  }

  public User setUsername(String username) {
    this.username = username;
    return this;
  }

  public String getPassword() {
    return password;
  }

  public User setPassword(String password) {
    this.password = password;
    return this;
  }

  public String[] getAuthorities() {
    return authorities.toArray(new String[authorities.size()]);
  }

  public User setAuthorities(String authority) {
    this.authorities.add(authority);
    return this;
  }

  @Override
  public String toString() {
    return "User{" +
        "username='" + username + '\'' +
        ", password='" + password + '\'' +
        ", authorities=" + authorities +
        '}';
  }
}