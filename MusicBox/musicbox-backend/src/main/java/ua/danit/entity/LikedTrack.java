package ua.danit.entity;

import com.google.common.base.MoreObjects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "liked_tracks")
public class LikedTrack {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "Track_ID")
  private String Track_ID;

  @Column(name = "User_ID")
  private String User_ID;

  public Long getId() {
    return id;
  }

  public String getTrack_ID() {
    return Track_ID;
  }

  public String getUser_ID() {
    return User_ID;
  }

  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("id", id)
        .add("Track_ID", Track_ID)
        .add("User_ID", User_ID)
        .toString();
  }
}