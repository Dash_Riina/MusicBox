package ua.danit.entity;

import com.google.common.base.MoreObjects;

import javax.persistence.*;
import java.util.List;

@Entity
@Table

public class Album {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name ="cover")
  private String cover;

  @OneToMany(mappedBy = "album")
  List<Track> trackList;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCover() {
    return cover;
  }

  public void setCover(String cover) {
    this.cover = cover;
  }

  public List<Track> getTrackList() {
    return trackList;
  }

  public void setTrackList(List<Track> trackList) {
    this.trackList = trackList;
  }

  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("id", id)
        .add("name", name)
        .add("cover", cover)
        .toString();
  }
}

// We can use also lombok with maven by adding dependencies to pom.xml
// using annotations and re-write like this

//@entity
//@Getter
//@Setter
//@Table(name = "albums")
//public class Album {
//  @Id
//  private Integer album_id;
//  private String album_name;
//  private String album_cover;
//}
