package ua.danit.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.MoreObjects;

import javax.persistence.*;

@Entity
public class Track {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String name;

  private String mp3;

  @ManyToOne
  @JoinColumn(name = "album_id")
  @JsonIgnore
  private Album album;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }



  public Album getAlbum() {
    return album;
  }

  public void setAlbum(Album album) {
    this.album = album;
  }

  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("id", id)
        .toString();
  }
}